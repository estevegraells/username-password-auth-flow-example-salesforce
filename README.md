# Uso del Flujo de Autenticación Username/Password Salesforce#

Este proyecto experimenta con el Flujo de Autenticación Username/Password de Salesforce.

### Objetivo ###

En cualquier compañía, en algún momento se plantea la necesidad de acceder a servicios expuestos en Salesforce, desde sistemas OnPremise o en el Cloud. 

El consumo de estos servicios requiere de un mecanismo de autenticación, para los cuales, Salesforce proporciona diferentes opciones llamadas Authentication Flows (https://help.salesforce.com/apex/HTViewHelpDoc?id=remoteaccess_authenticate.htm&language=en_US).

En este repo, proporciono un ejemplo del Flow más simple de todos ellos, el Username/Password Flow (aunque simple, es interesante que repases los otros, dados que este flujo requiere proporcionar credenciales al sistema Legacy). 

Destacar que, en el apartado, **Requerimientos**, detallo todos los requerimientos previos.

### Requerimientos ###
* Debe crearse una Connected App 
* Debe crearse un usuario (idealmente sólo usado para Integración entre los sistemas)
* Finalmente se construye la petición y se realiza un POST bajo ciertos criterios 

### Bibliografía ###
Para entender este mecanismo de autenticación hay que leer con detenimiento la siguiente bibligrafía:

* Autenticating Apps with OAuth: https://help.salesforce.com/apex/HTViewHelpDoc?id=remoteaccess_authenticate.htm&language=en_US
* OAuth 2.0 Username/Password Flow: https://help.salesforce.com/apex/HTViewHelpDoc?id=remoteaccess_oauth_username_password_flow.htm&language=en_US
* Creating a Connected App: https://help.salesforce.com/apex/HTViewHelpDoc?id=connected_app_create.htm

Además:

* Se requiere un conocimiento básico de cómo realizar una llamada POST HTTP, con cabecereas y cuerpo.
* No es necesario un conocimiento del protocolo Oauth2, aunque es recomendable para entender los mecanismos de la plataforma
 
### Sobre mí ###

* Arquitecto Salesforce en Gas Natural Fenosa: [esteve.graells@gmail.com](Link mailto:esteve.graells@gmail.com)