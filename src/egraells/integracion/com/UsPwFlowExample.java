package egraells.integracion.com;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class UsPwFlowExample {
	
    private static String CONSUMER_KEY, CONSUMER_SECRET, USERNAME, PASSWORD, DEVTOKEN;
    
    //Obtener las credenciales de  fichero de propiedades externo, para evitar mostrarlas directamente en el c�digo
    private static void getCredenciales(){
    	Properties prop = new Properties();
    	InputStream input = null;

    	try {
    		input = new FileInputStream("C:/Users/Esteve Graells/OneDrive - GAS Natural Inform�tica, S.A/GNF/workspace/UsPwFlow/src/egraells/integracion/com/credenciales.propierties");
    		// load a properties file
    		prop.load(input);

    		// get the property value and print it out
    		CONSUMER_KEY 	= prop.getProperty("CONSUMER_KEY");
    		CONSUMER_SECRET = prop.getProperty("CONSUMER_SECRET");
    		USERNAME 		= prop.getProperty("USERNAME");
    		PASSWORD 		= prop.getProperty("PASSWORD");
    		DEVTOKEN 		= prop.getProperty("DEVTOKEN");

    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

	public static void main(String[] args) {
		
		getCredenciales();
		
		//Endpoint destino
		try{
			String url = "https://login.salesforce.com/services/oauth2/token";
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	
			//Cabeceras - las mínimas posibles
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	
			//Parámetros requeridos por el token JWT con el token firmado
			String urlParameters   = "grant_type=password";
					urlParameters += "&client_id=" + CONSUMER_KEY;
					urlParameters += "&client_secret=" + CONSUMER_SECRET;
					urlParameters += "&username=" + USERNAME;
					urlParameters += "&password=" + PASSWORD + DEVTOKEN;
	
			// Envio
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
	
			//Obtención de resultados y log en la consola
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
	
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
			// print result
			System.out.println(response.toString());

		//Esto debería ser mucho más elaborado ;-)
		}catch (Exception e){
			e.printStackTrace();
		}
		
	
	}

}
